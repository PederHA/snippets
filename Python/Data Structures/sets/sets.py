set1 = {1, 2, 3}
set2 = {3, 4, 5}

# Numbers in both set1 and set2
print(set1 | set2) # {1, 2, 3, 4, 5}

# Numbers in both set1 and set2
print(set1 & set2) # {3}

# Numbers in set1 or set2, but noth both
print(set1 ^ set2) # {1, 2, 4, 5}

# Numbers in set1 but not in set2
print(set1 - set2) # {1, 2}

# Numbers in set2 but not in set1
print(set2 - set1) # {4, 5}

# New set from two different sets
set3 = set1 | set2
print(set3) # {1, 2, 3, 4, 5}

# Use class method `.update()`
set1.update(set2)
print(set1) # {1, 2, 3, 4, 5}
