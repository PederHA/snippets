import itertools

set_a = {"a", "b", "c"}
set_1 = {1, 2, 3}

for product in itertools.product(set_a, set_1, repeat=1):
    print(product)
