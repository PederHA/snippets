"""
Example demonstrating how to implement custom dunder methods
to a class that subclasses `set`.
"""

class _Set(set):
    def __add__(self: set, other:set):
        if isinstance(other, _Set):
            union_sets = self.union(other)
            new_set = _Set()
            new_set.update(union_sets)
            return new_set
        else:
            raise NotImplementedError
    
    def __repr__(self):
        output = "{"
        n = 0
        for i in self:
            n += 1
            output += f"{str(i)}"
            if n < len(self):
                output+=", "
        else:
            output += "}"
        return output          

set1 = _Set()
set1.update({1,2,3,4,5})
set2 = _Set()
set2.update({4,5,6,7,8})
set3 = set1 + set2
print(set3) # {1, 2, 3, 4, 5, 6, 7, 8}