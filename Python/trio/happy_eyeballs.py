import trio

async def open_tcp_socket(hostname, port, *, max_wait_time=0.250):
    targets = await trio.socket.getaddrinfo(hostname, port, type=trio.socket.SOCK_STREAM)
    failed_attempts = [trio.Event() for _ in targets]
    winning_socket = None

    async def attempt(target_idx, nursery):
        if target_idx > 0:
            with trio.move_on_after(max_wait_time):
                await failed_attempts[target_idx - 1].wait()
        if target_idx + 1 < len(targets):
            nursery.start_soon(attempt, target_idx + 1, nursery)
        
        try:
            *socket_config, _, target = targets[target_idx]
            socket = trio.socket.socket(*socket_config)
            await socket.connect(target)

        except OSError:
            failed_attempts[target_idx].set()
        else:
            nonlocal winning_socket
            winning_socket = socket
            print(f"Socket with index {target_idx} won. Cancelling all other connection attempts.")
            nursery.cancel_scope.cancel()
    
    async with trio.open_nursery() as nursery:
        nursery.start_soon(attempt, 0, nursery)

    if winning_socket is None:
        raise OSError("Yikes")
    else:
        return winning_socket

async def main():
    print(await open_tcp_socket("debian.org", "https"))

trio.run(main)