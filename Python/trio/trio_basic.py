import trio

async def sleep_some(sleep_duration: int) -> None:
    print(f"Sleeping for {sleep_duration} seconds...")
    await trio.sleep(sleep_duration)
    print(f"Woke up after {sleep_duration} seconds!")

async def start_tasks():
    async with trio.open_nursery() as nursery:
        nursery.start_soon(sleep_some, 3)
        nursery.start_soon(sleep_some, 5)

trio.run(start_tasks)