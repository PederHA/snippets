def generator_func(n: int) -> str:
    for m in range(n, n+n, 1):
        yield chr(m)

for char in generator_func(48):
    print(char," ", end="")

# Output: 0  1  2  3  4  5  6  7  8  9  :  ;  <  =  >  ?  @  A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  [  \  ]  ^  _