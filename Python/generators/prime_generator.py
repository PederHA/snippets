def prime(max):
    number = 1
    def check_prime(number):
        for divisor in range(2, int(number ** 0.5) + 1):
            if number % divisor == 0:
                return False
        return True
    while number <= max:
        if check_prime(number):
            yield number
        number += 1

for prime_num in prime(500):
    print(prime_num)