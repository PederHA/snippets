"""
Basic Qt / PySide2 example featuring a text field, push button
and custom window icon. Clicking the push button passes
the text field input to `validate_input()`.

`validate_input()` is easy to configure, and is used to
define valid text field input values.


"""

import sys
from PySide2.QtWidgets import *
from PySide2.QtGui import QFont, QIcon, QPixmap


class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.default_input = "Example input"


        # Window title
        self.setWindowTitle("Add Device")

        # Window icon
        self.setWindowIcon(QIcon(QPixmap("Python/Qt/icons/chip.png")))

        # Text field label
        self.input_label = QLabel("Input")
        self.input_label.setFont(QFont("Arial", 8, QFont.Bold))
        # Text field
        self.input = QLineEdit()
        self.input.setPlaceholderText(self.default_input)

        # Push button
        self.button = QPushButton("Push button")
        self.button.clicked.connect(self.validate_input)
        
        # Create Layout object
        layout = QVBoxLayout() 
        layout.addWidget(self.input_label)
        layout.addWidget(self.input)  
        layout.addWidget(self.button)
        layout.setMargin(20)

        # Set dialog layout with `layout` object
        self.setLayout(layout)

    
    def validate_input(self) -> None:
        """
        Compares list of default field values to user input.
        """

        self.text_field_entries = [self.input.text()] # Ideally more than 1 object in list
        self.default_entries = [""]

        # Checks if all fields have been filled in, by comparing field values to default values.
        valid_args = False
        for word in self.text_field_entries:
            if word in self.default_entries:
                break
        else:
            valid_args = True

        if valid_args:
            # If opt_info field is empty, the `.text()` method returns "".
            # A new variable opt_info is set to None if `.text()` returns "".
            self.message_box = QMessageBox.information(self, "Success", "Input is valid!", QMessageBox.Ok)   
        
        else:
            self.message_box = QMessageBox.warning(self, "Warning", "Every field must be filled out.", QMessageBox.Ok)

    

if __name__ == '__main__':
    # Create the Qt Application
    app = QApplication(sys.argv)
    # Create and show the form
    form = Form()
    form.setFixedWidth(350)
    #form.setFixedHeight(400)
    form.show()
    # Run the main Qt loop
    sys.exit(app.exec_())
