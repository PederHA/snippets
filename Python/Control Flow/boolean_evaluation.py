def check_evaluation(obj) -> None:
    if obj:
        print(f"{obj} == True")
    else:
        print(f"{obj} == False")

check_evaluation([]) # False
check_evaluation([1,2,3]) # True
check_evaluation(0) # False
check_evaluation(1) # True
check_evaluation(-1) # True