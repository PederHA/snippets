import matplotlib.pyplot as plt
import numpy as np

groups, scores = np.arange(5), (5,15,12,19,9)
plt.bar(groups, scores)
plt.xlabel("Group")
plt.ylabel("Scores")
plt.title("Scores by group")

# Save image file to disk
plt.savefig("fig.png")

# Show in viewer
plt.show()

