"""
Passing the event loop into the running async function in order to stop the
event loop if a given condition is met.
"""

import asyncio
from typing import Iterable

letters_upper = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

symbols = [' ', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@','[', '\\', ']', '^', '_', '`', '{', '|', '}', '~']

async def iterate_something(something: Iterable, delay: float, loop):
    remaining_idx = len(something)
    for thing in something:
        print()
        print(thing)
        remaining_idx -= 1
        print(f"Remaining indices in iterable: {remaining_idx}")
        await asyncio.sleep(delay)
    else:
        print("Reached end of iterable. Stopping event loop.")
        loop.stop()

loop = asyncio.get_event_loop()
loop.create_task(iterate_something(letters_upper, 0.2, loop))
loop.create_task(iterate_something(symbols, 0.5, loop))
loop.run_forever()

print("-------------------------------")
print("This is outside the event loop!")
print("-------------------------------")