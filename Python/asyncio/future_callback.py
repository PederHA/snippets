"""
Destroy competing tasks after fastest task completes using `asyncio.Future`
"""

import asyncio
import random

sleep_durations = []

async def some_func(n: int, future) -> None:
    sleep_duration = random.randint(1,6)
    # Prevents several loops sharing identical sleep durations
    while sleep_duration in sleep_durations:
        sleep_duration = random.randint(1,6)
    sleep_durations.append(sleep_duration)
    print(f"Loop {n+1}: Sleeping for {sleep_duration} seconds...")
    await asyncio.sleep(sleep_duration)
    print(f"\nLoop {n+1}: Woke up after {sleep_duration} seconds!")
    future.set_result(f"Fastest wakeup was loop {n+1} after {sleep_duration} seconds!\n")

def got_result(future):
    print(future.result())
    loop.stop()    

loop = asyncio.get_event_loop()
future = asyncio.Future()
for n in range(5):
    asyncio.ensure_future(some_func(n, future))
future.add_done_callback(got_result)
try:
    loop.run_forever()
finally:
    loop.close()
