"""
Example using `asyncio.ensure_future`
"""

import asyncio
import random

async def some_func(n: int) -> None:
    sleep_duration = random.randint(1,6)
    print(f"Loop {n+1}: Sleeping for {sleep_duration} seconds...")
    await asyncio.sleep(sleep_duration)
    print(f"Loop {n+1}: Woke up after {sleep_duration} seconds!")    

def main():
    requests = []
    for n in range(5):
        requests.append(asyncio.ensure_future(some_func(n)))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(*requests))
    loop.close()

main()