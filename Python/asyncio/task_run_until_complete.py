import asyncio

async def something(loop):
    print("LUL")
    print(loop.is_running())

loop = asyncio.get_event_loop()

# `AbstractEventLoop.create_task()` returns a Task object
some_task = loop.create_task(something(loop))

# The task is run once, until completion
loop.run_until_complete(some_task)
