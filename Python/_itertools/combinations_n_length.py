import itertools

list_1 = ["n0tail", "ana", "ceb", "jerax", "topson"]
list_2 = ["matumbaman", "miracle", "kuroky", "gh", "mind_control"]
# The two lists need to be combined, because passing in both separately to
# itertools.product yields tuples of a length that is a multiple of 2.
combined_teams = list_1 + list_2


for team in itertools.product(combined_teams, repeat=5):
    print(team)