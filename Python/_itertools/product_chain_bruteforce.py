
from itertools import chain, product
import os
import sys

letters_lower = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
'z']

def bruteforce3(charset: list, maxlength: int) -> str:
    return (''.join(candidate)
        for candidate in chain.from_iterable(product(charset, repeat=i)
        for i in range(1, maxlength + 1)))

for attempt in bruteforce3(letters_lower, 5):
    print(attempt)
    if attempt == "abc":
        matched = True
    else:
        matched = False
    if matched:
        break