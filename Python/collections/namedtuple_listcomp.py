from collections import namedtuple
import random

Player = namedtuple("player", "name rating wins losses")
names = ["Torvald", "Simon", "Peder", "Jon", "Ferdy", "Julius", "Aslak"]

def create_player(name):
    wins = random.randint(1,10)
    losses = random.randint(1,10)
    rating = (1000 + 25*wins - 25*losses)
    return Player(name, rating, wins, losses)

players = []
for name in names:
    players.append(create_player(name))

l = [player.name for player in players]
print(l)