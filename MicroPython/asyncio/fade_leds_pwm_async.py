"""
Fade LEDs with PWM asynchronously 
"""

from uasyncio import sleep_ms, get_event_loop
from machine import Pin, PWM

yellow_led = PWM(Pin(4, Pin.OUT))
red_led = PWM(Pin(12, Pin.OUT))

async def flash_led(led, event_loop, delay_ms):
    for i in range(1024):
        led.duty(i)
        await sleep_ms(delay_ms)
    else:
        event_loop.stop()

loop = get_event_loop()
loop.create_task(flash_led(yellow_led, loop, 5))
loop.create_task(flash_led(red_led, loop, 15))
loop.run_forever()

yellow_led.duty(0)
red_led.duty(0)