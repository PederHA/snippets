"""
MicroPython Asyncio example demonstrating how to listen for a 
button press in order to stop the event loop.
"""

from uasyncio import sleep_ms, get_event_loop
from machine import Pin, PWM

yellow_led = PWM(Pin(4, Pin.OUT))
red_led = PWM(Pin(12, Pin.OUT))
push_button = Pin(14, Pin.IN, Pin.PULL_UP)

async def flash_led(led, event_loop, delay_ms):
    for i in range(1024):
        led.duty(i)
        await sleep_ms(delay_ms)
    else:
        event_loop.stop()

async def listen_button(button, event_loop):
    while True:
        if not button.value():
            event_loop.stop()
        await sleep_ms(2)

loop = get_event_loop()
loop.create_task(flash_led(yellow_led, loop, 5))
loop.create_task(flash_led(red_led, loop, 15))
loop.create_task(listen_button(push_button, loop))
loop.run_forever()

yellow_led.duty(0)
red_led.duty(0)