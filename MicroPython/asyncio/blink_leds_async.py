"""
Asyncio example
"""

from uasyncio import sleep_ms, get_event_loop
from machine import Pin

yellow_led = Pin(4, Pin.OUT)
yellow_led(0)
red_led = Pin(12, Pin.OUT)
red_led(0)

async def flash_led(led, event_loop, delay_ms):
    for i in range(50):
        led(1)
        await sleep_ms(delay_ms)
        led(0)
        await sleep_ms(delay_ms)
    else:
        event_loop.stop()

loop = get_event_loop()
loop.create_task(flash_led(yellow_led, loop, 200))
loop.create_task(flash_led(red_led, loop, 50))
loop.run_forever()

yellow_led(0)
red_led(0)